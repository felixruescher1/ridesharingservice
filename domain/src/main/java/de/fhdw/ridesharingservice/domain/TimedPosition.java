package de.fhdw.ridesharingservice.domain;

import java.time.Instant;

/**
 * Eine Position, die mit einem Zeitstempel versehen ist.
 */
public class TimedPosition {

    private int id;
    private Position position;
    private Instant timestamp;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public TimedPosition() {
    }

    /**
     * Konstruktor.
     */
    public TimedPosition(final Position position, final Instant timestamp) {
        this.position = position;
        this.timestamp = timestamp;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert die Position.
     */
    public Position getPosition() {
        return this.position;
    }

    /**
     * Liefert den Zeitstempel.
     */
    public Instant getTimestamp() {
        return this.timestamp;
    }
}
