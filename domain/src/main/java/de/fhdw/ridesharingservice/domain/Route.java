package de.fhdw.ridesharingservice.domain;

import java.util.List;

/**
 * Eine Route als Zusammenfassung mehrerer {@link Order Buchungen}.
 */
public class Route {

    private int id;
    private Vehicle vehicle;
    private List<Order> orders;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Route() {
    }

    /**
     * Konstruktor.
     */
    public Route(
            final Vehicle vehicle,
            final List<Order> orders) {
        this.vehicle = vehicle;
        this.orders = orders;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert das Fahrzeug.
     */
    public Vehicle getVehicle() {
        return this.vehicle;
    }

    /**
     * Liefert die Buchungen.
     */
    public List<Order> getOrders() {
        return this.orders;
    }
}
