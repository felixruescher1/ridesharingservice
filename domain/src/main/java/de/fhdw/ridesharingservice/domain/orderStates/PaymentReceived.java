package de.fhdw.ridesharingservice.domain.orderStates;

import de.fhdw.ridesharingservice.domain.Order;
import de.fhdw.ridesharingservice.domain.OrderState;

/**
 * Repräsentiert den Zustand 'Bezahlt'.
 */
public class PaymentReceived  extends OrderState {

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public PaymentReceived() {
    }

    /**
     * Konstruktor.
     */
    protected PaymentReceived(final Order order) {
        super(order);
    }

    @Override
    public void acceptVisitor(OrderStateVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    protected void accept() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();

    }

    @Override
    protected void cancel() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();

    }

    @Override
    protected void pay() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();

    }

    @Override
    protected void close() throws InconsistentOrderStateTransitionException {
        this.getOrder().setState(new Closed(this.getOrder()));

    }
}
