package de.fhdw.ridesharingservice.domain.orderStates;

import de.fhdw.ridesharingservice.domain.Order;
import de.fhdw.ridesharingservice.domain.OrderState;

/**
 * Repräsentiert den Zustand 'Ausstehend'. Damit ist die zugehörige {@link Order Buchung} noch ein 'Vorschlag'.
 */
public class Pending extends OrderState {

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Pending() {
    }

    /**
     * Konstruktor.
     */
    protected Pending(final Order order) {
        super(order);
    }

    @Override
    public void acceptVisitor(OrderStateVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    protected void accept() throws InconsistentOrderStateTransitionException {
        this.getOrder().setState(new Accepted(this.getOrder()));
    }

    @Override
    protected void cancel() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();
    }

    @Override
    protected void pay() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();
    }

    @Override
    protected void close() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();
    }
}
