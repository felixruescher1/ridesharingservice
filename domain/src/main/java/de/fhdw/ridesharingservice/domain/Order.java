package de.fhdw.ridesharingservice.domain;

import de.fhdw.ridesharingservice.domain.orderStates.*;

import java.time.Instant;
import java.util.List;

/**
 * Eine Buchung.
 */
public class Order {

    private int id;
    private Position start;
    private Position destination;

    private List<OrderState> history;
    private OrderState currentState;

    private Customer customer;
    private Instant startTime;
    private Instant endTime;
    private int bookedSeats;
    private double price;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Order() {
    }

    /**
     * Konstruktor.
     */
    public Order(
            final Position start,
            final Position destination,
            final OrderState currentState,
            final List<OrderState> history,
            final Customer customer,
            final Instant startTime,
            final Instant endTime,
            final int bookedSeats,
            final double price) {
        this.start = start;
        this.destination = destination;
        this.currentState = currentState;
        this.history = history;
        this.customer = customer;
        this.startTime = startTime;
        this.endTime = endTime;
        this.bookedSeats = bookedSeats;
        this.price = price;
    }

    /**
     * Akzeptiert die Buchung.
     */
    public final void accept() throws OrderState.InconsistentOrderStateTransitionException {
        this.currentState.accept();
    }

    /**
     * Storniert die Buchung.
     */
    public final void cancel() throws OrderState.InconsistentOrderStateTransitionException {
        this.currentState.cancel();
    }

    /**
     * Bezahlt die Buchung.
     */
    public final void pay() throws OrderState.InconsistentOrderStateTransitionException {
        this.currentState.pay();
    }

    /**
     * Schließt die Buchung ab.
     */
    public final void close() throws OrderState.InconsistentOrderStateTransitionException {
        this.currentState.close();
    }

    public final void setState(final OrderState state) {
        this.history.add(this.currentState);
        this.currentState = state;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert die Start-Koordinaten.
     */
    public Position getStart() {
        return this.start;
    }

    /**
     * Liefert die Ziel-Koordinaten.
     */
    public Position getDestination() {
        return this.destination;
    }

    /**
     * Liefert die Historie der Zustände.
     */
    public List<OrderState> getHistory() {
        return this.history;
    }

    /**
     * Liefert den aktuellen Zustand.
     */
    public OrderState getCurrentState() {
        return this.currentState;
    }

    /**
     * Liefert den Kunden.
     */
    public Customer getCustomer() {
        return this.customer;
    }

    /**
     * Liefert den Startzeitpunkt.
     */
    public Instant getStartTime() {
        return this.startTime;
    }

    /**
     * Liefert den Endzeitpunkt.
     */
    public Instant getEndTime() {
        return this.endTime;
    }

    /**
     * Liefert die Anzahl an gebuchten Sitzen.
     */
    public int getBookedSeats() {
        return this.bookedSeats;
    }

    /**
     * Liefert den Preis.
     */
    public double getPrice() {
        return this.price;
    }
}
