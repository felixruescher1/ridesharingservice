package de.fhdw.ridesharingservice.domain;

import java.time.Instant;

/**
 * Ein Fahrzeug.
 */
public class Vehicle {

    private int id;
    private String vin;
    private String licenseNumber;
    private int seats;
    private Instant initialRegistration;
    private Double price;
    private String brand;
    private EngineType engineType;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Vehicle() {
    }

    /**
     * Konstruktor.
     */
    public Vehicle(
            final String vin,
            final String licenseNumber,
            final int seats,
            final Instant initialRegistration,
            final Double price,
            final String brand,
            final EngineType engineType) {
        this.vin = vin;
        this.licenseNumber = licenseNumber;
        this.seats = seats;
        this.initialRegistration = initialRegistration;
        this.price = price;
        this.brand = brand;
        this.engineType = engineType;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert die Fahrzeug-Identifikations-Nummer.
     */
    public String getVin() {
        return this.vin;
    }

    /**
     * Liefert das Kennzeichen.
     */
    public String getLicenseNumber() {
        return this.licenseNumber;
    }

    /**
     * Liefert die Anzahl an Sitzen.
     */
    public int getSeats() {
        return this.seats;
    }

    /**
     * Liefert den Zeitpunkt der initialen Registrierung.
     */
    public Instant getInitialRegistration() {
        return this.initialRegistration;
    }

    /**
     * Liefert den Kaufpreis.
     */
    public Double getPrice() {
        return this.price;
    }

    /**
     * Liefert die Marke.
     */
    public String getBrand() {
        return this.brand;
    }

    /**
     * Liefert die Antriebsart.
     */
    public EngineType getEngineType() {
        return this.engineType;
    }
}
