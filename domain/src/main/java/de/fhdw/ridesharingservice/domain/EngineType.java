package de.fhdw.ridesharingservice.domain;

public enum EngineType {
    DIESEL,
    PETROL,
    ELECTRIC,
    HYDROGEN,
    GAS,
    HYBRID
}
