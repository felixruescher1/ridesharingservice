package de.fhdw.ridesharingservice.domain.orderStates;

import de.fhdw.ridesharingservice.domain.Order;
import de.fhdw.ridesharingservice.domain.OrderState;

/**
 * Repräsentiert den Zustand 'Akzeptiert'. Damit ist die zugehörige {@link Order Buchung} eine 'echte Buchung'.
 */
public class Accepted extends OrderState {

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Accepted() {
    }

    /**
     * Konstruktor.
     */
    protected Accepted(final Order order) {
        super(order);
    }

    @Override
    public void acceptVisitor(OrderStateVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    protected void accept() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();
    }

    @Override
    protected void cancel() throws InconsistentOrderStateTransitionException {
        this.getOrder().setState(new Canceled(this.getOrder()));
    }

    @Override
    protected void pay() throws InconsistentOrderStateTransitionException {
        this.getOrder().setState(new PaymentReceived(this.getOrder()));
    }

    @Override
    protected void close() throws InconsistentOrderStateTransitionException {
        throw new InconsistentOrderStateTransitionException();
    }
}
