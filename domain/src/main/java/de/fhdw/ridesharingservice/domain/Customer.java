package de.fhdw.ridesharingservice.domain;

import java.util.List;

/**
 * Ein Kunde.
 */
public class Customer {

    private int id;
    private int customerId;
    private String firstName;
    private String lastName;
    private List<Order> orders;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Customer() {
    }

    /**
     * Konstruktor.
     */
    public Customer(
            final int customerId,
            final String firstName,
            final String lastName,
            final List<Order> orders) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.orders = orders;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert die Kundennummer / Kunden-ID.
     */
    public int getCustomerId() {
        return this.customerId;
    }

    /**
     * Liefert den Vornamen.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Liefert den Nachnamen.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Liefert die Buchungen.
     */
    public List<Order> getOrders() {
        return this.orders;
    }
}
