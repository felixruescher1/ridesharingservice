package de.fhdw.ridesharingservice.domain;

/**
 * Eine Position mit den Geo-Koordinaten {@code latitude} und {@code longitude}.
 */
public class Position {

    private int id;
    private int latitude;
    private int longitude;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public Position() {
    }

    /**
     * Konstruktor.
     */
    public Position(final int latitude, final int longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Liefert die Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Liefert die Latitude (Breitengrad).
     */
    public int getLatitude() {
        return this.latitude;
    }

    /**
     * Liefert die Longitude (Längengrad).
     */
    public int getLongitude() {
        return this.longitude;
    }
}


