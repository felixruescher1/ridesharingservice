package de.fhdw.ridesharingservice.domain;

import de.fhdw.ridesharingservice.domain.orderStates.*;

/**
 * Ein Zustand einer {@link Order Buchung}.
 */
public abstract class OrderState {

    private int id;
    private Order order;

    /**
     * Parameterloser Konstruktor (für Hibernate).
     */
    public OrderState() {
    }

    /**
     * Konstruktor.
     */
    protected OrderState(final Order order) {
        this.order = order;
    }

    public abstract void acceptVisitor(final OrderStateVisitor visitor);

    protected Order getOrder() {
        return order;
    }

    protected abstract void accept() throws InconsistentOrderStateTransitionException;
    protected abstract void cancel() throws InconsistentOrderStateTransitionException;
    protected abstract void pay() throws InconsistentOrderStateTransitionException;
    protected abstract void close() throws InconsistentOrderStateTransitionException;

    public static class InconsistentOrderStateTransitionException extends Exception {

        /**
         * Konstruktor.
         */
        public InconsistentOrderStateTransitionException() {
        }

        /**
         * Konstruktor.
         */
        public InconsistentOrderStateTransitionException(final String message) {
            super(message);
        }
    }

    public interface OrderStateVisitor {

        public abstract void visit(final Accepted accepted);
        public abstract void visit(final Canceled canceled);
        public abstract void visit(final Closed closed);
        public abstract void visit(final PaymentReceived paymentReceived);
        public abstract void visit(final Pending pending);
    }
}
