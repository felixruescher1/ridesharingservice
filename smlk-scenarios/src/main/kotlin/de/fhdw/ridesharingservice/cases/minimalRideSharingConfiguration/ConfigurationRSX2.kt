package de.fhdw.ridesharingservice.cases.minimalRideSharingConfiguration

import de.fhdw.ridesharingservice.classes.*
import de.fhdw.ridesharingservice.scenarios.vehicleRsClientScenarios
import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.label
import org.scenriotools.smlk.animator.smlkextension.shadowScenario

class ConfigurationRSX2 {
    companion object{

        val admin = Customer("Admin", 500.0, 400.0)

        val frontend = Frontend("Frontend", 500.0, 200.0)

        val backend = Backend("Backend", 900.0, 200.0)

        val database = Database("Database", 900.0, 400.0)

        val environmentMessageTypes = listOf(
            Frontend::getInformationOfAllVehicles)
    }
}

fun getInformationOfAllVehiclesConfiguration() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX2.frontend.dependencies.add(ConfigurationRSX2.backend) // Visuelle Unterstützung
    ConfigurationRSX2.frontend.backend = ConfigurationRSX2.backend

    ConfigurationRSX2.backend.database = ConfigurationRSX2.database
    ConfigurationRSX2.backend.dependencies.add(ConfigurationRSX2.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX2.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX2.admin,
            ConfigurationRSX2.frontend,
            ConfigurationRSX2.backend,
            ConfigurationRSX2.database),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX2.admin sends ConfigurationRSX2.frontend.getInformationOfAllVehicles())
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Send information of all vehicles" to configuration
}
