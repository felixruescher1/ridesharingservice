package de.fhdw.ridesharingservice.cases.minimalRideSharingConfiguration

import de.fhdw.ridesharingservice.classes.*
import de.fhdw.ridesharingservice.scenarios.vehicleRsClientScenarios
import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.label
import org.scenriotools.smlk.animator.smlkextension.shadowScenario

class ConfigurationRSX12 {

    companion object{

        val vehicle1 = Vehicle(1, 300.0, 50.0, 4)
        val vehicleRsClient1 = VehicleRSClient(1, 550.0, 50.0)

        val vehicle2 = Vehicle(2, 300.0, 200.0, 4)
        val vehicleRsClient2 = VehicleRSClient(2, 550.0, 200.0)

        val vehicle3 = Vehicle(3, 300.0, 350.0, 4)
        val vehicleRsClient3 = VehicleRSClient(3, 550.0, 350.0)

        val customer1 = Customer("Hugo", 300.0, 400.0)

        val frontend = Frontend("Frontend", 500.0, 550.0)

        val backend = Backend("Backend", 900.0, 200.0)

        val database = Database("Database", 900.0, 400.0)

        val environmentMessageTypes = listOf(
            VehicleRSClient::currentPosition,
            Frontend::getPositionOfRide)
    }
}

fun customerRequestingNotExistingVehiclePosition() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX12.customer1.dependencies.add(ConfigurationRSX12.frontend) // Visuelle Unterstützung

    ConfigurationRSX12.frontend.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.frontend.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.backend.database = ConfigurationRSX12.database
    ConfigurationRSX12.backend.dependencies.add(ConfigurationRSX12.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX12.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX12.customer1,
            ConfigurationRSX12.frontend,
            ConfigurationRSX12.backend,
            ConfigurationRSX12.database),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX12.customer1 sends ConfigurationRSX12.frontend.getPositionOfRide(1))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Requesting not existing vehicle position" to configuration
}

fun customerRequestingExistingVehiclePosition() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX12.customer1.dependencies.add(ConfigurationRSX12.frontend) // Visuelle Unterstützung

    ConfigurationRSX12.frontend.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.frontend.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.backend.database = ConfigurationRSX12.database
    ConfigurationRSX12.backend.dependencies.add(ConfigurationRSX12.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX12.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX12.vehicle1,
            ConfigurationRSX12.vehicleRsClient1,
            ConfigurationRSX12.customer1,
            ConfigurationRSX12.frontend,
            ConfigurationRSX12.backend,
            ConfigurationRSX12.database),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(10, 10))
            request(ConfigurationRSX12.customer1 sends ConfigurationRSX12.frontend.getPositionOfRide(1))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Requesting existing vehicle position" to configuration
}

fun singleVehicleSendingPositionsConfiguration() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX12.vehicle1.dependencies.add(ConfigurationRSX12.vehicleRsClient1) // Visuelle Unterstützung

    ConfigurationRSX12.vehicleRsClient1.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.vehicleRsClient1.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.backend.database = ConfigurationRSX12.database
    ConfigurationRSX12.backend.dependencies.add(ConfigurationRSX12.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX12.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX12.backend,
            ConfigurationRSX12.database,
            ConfigurationRSX12.vehicle1,
            ConfigurationRSX12.vehicleRsClient1),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(10, 10))
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(12, 13))
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(14, 20))
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(14, 17))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Persist positions of single vehicle" to configuration
}

fun multipleVehicleSendingPositionsConfiguration() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX12.vehicle1.dependencies.add(ConfigurationRSX12.vehicleRsClient1) // Visuelle Unterstützung
    ConfigurationRSX12.vehicle2.dependencies.add(ConfigurationRSX12.vehicleRsClient2) // Visuelle Unterstützung
    ConfigurationRSX12.vehicle3.dependencies.add(ConfigurationRSX12.vehicleRsClient3) // Visuelle Unterstützung

    ConfigurationRSX12.vehicleRsClient1.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.vehicleRsClient1.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.vehicleRsClient2.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.vehicleRsClient2.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.vehicleRsClient3.backend = ConfigurationRSX12.backend
    ConfigurationRSX12.vehicleRsClient3.dependencies.add(ConfigurationRSX12.backend) // Visuelle Unterstützung

    ConfigurationRSX12.backend.database = ConfigurationRSX12.database
    ConfigurationRSX12.backend.dependencies.add(ConfigurationRSX12.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX12.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX12.backend,
            ConfigurationRSX12.database,
            ConfigurationRSX12.vehicle1,
            ConfigurationRSX12.vehicleRsClient1,
            ConfigurationRSX12.vehicle2,
            ConfigurationRSX12.vehicleRsClient2,
            ConfigurationRSX12.vehicle3,
            ConfigurationRSX12.vehicleRsClient3),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(10, 10))
            request(ConfigurationRSX12.vehicle2 sends ConfigurationRSX12.vehicleRsClient2.currentPosition(12, 13))
            request(ConfigurationRSX12.vehicle1 sends ConfigurationRSX12.vehicleRsClient1.currentPosition(14, 20))
            request(ConfigurationRSX12.vehicle3 sends ConfigurationRSX12.vehicleRsClient3.currentPosition(14, 17))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Persist positions of multiple vehicles" to configuration
}
