package de.fhdw.ridesharingservice.cases

import de.fhdw.ridesharingservice.cases.minimalRideSharingConfiguration.*
import de.fhdw.ridesharingservice.cases.userLoginConfiguration.userLoginFlowConfiguration
import de.fhdw.ridesharingservice.cases.userLoginConfiguration.userRegistrationFlowConfiguration
import org.scenriotools.smlk.animator.model.InteractionSystemConfigurations

val configurations = InteractionSystemConfigurations(
        mapOf(
                customerRequestingNotExistingVehiclePosition(),
                customerRequestingExistingVehiclePosition(),
                singleVehicleSendingPositionsConfiguration(),
                multipleVehicleSendingPositionsConfiguration(),
                userLoginFlowConfiguration(),
                userRegistrationFlowConfiguration(),
                multipleVehicleSendingPositionsConfiguration(),
                getInformationOfAllVehiclesConfiguration(),
                customerRequestingOfferAndBooking(),
                customerRequestingOfferAndRejecting(),
        )
)
