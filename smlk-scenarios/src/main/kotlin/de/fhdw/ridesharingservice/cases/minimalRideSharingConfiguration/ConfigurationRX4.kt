package de.fhdw.ridesharingservice.cases.minimalRideSharingConfiguration

import de.fhdw.ridesharingservice.classes.*
import de.fhdw.ridesharingservice.scenarios.vehicleRsClientScenarios
import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.label
import org.scenriotools.smlk.animator.smlkextension.shadowScenario
import java.util.*

class ConfigurationRSX4 {

    companion object{

        val customer1 = Customer("Hugo", 300.0, 400.0)

        val frontend = Frontend("Frontend", 500.0, 550.0)

        val backend = Backend("Backend", 900.0, 200.0)

        val database = Database("Database", 900.0, 400.0)

        val startPos: Position = Position(50, 50)

        val destination: Position = Position(100, 75)

        val environmentMessageTypes = listOf(
            Frontend::requestOffer,
            Frontend::bookOffer,
            Frontend::rejectOffer,
            )
    }
}

fun customerRequestingOfferAndBooking() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX4.customer1.dependencies.add(ConfigurationRSX4.frontend) // Visuelle Unterstützung

    ConfigurationRSX4.frontend.backend = ConfigurationRSX4.backend
    ConfigurationRSX4.frontend.dependencies.add(ConfigurationRSX4.backend) // Visuelle Unterstützung

    ConfigurationRSX4.backend.database = ConfigurationRSX4.database
    ConfigurationRSX4.backend.dependencies.add(ConfigurationRSX4.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX4.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX4.customer1,
            ConfigurationRSX4.frontend,
            ConfigurationRSX4.backend,
            ConfigurationRSX4.database),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX4.customer1 sends ConfigurationRSX4.frontend.requestOffer(
                ConfigurationRSX4.startPos,
                ConfigurationRSX4.destination,
                1,
                ConfigurationRSX4.customer1.name,
            ))
            val responseEvent = waitFor(ConfigurationRSX4.frontend sends ConfigurationRSX4.customer1 receives ICustomer::frontendOfferResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            request(ConfigurationRSX4.customer1 sends ConfigurationRSX4.frontend.bookOffer(responseValue))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Requesting ride offer and book it" to configuration
}

fun customerRequestingOfferAndRejecting() : Pair<String, InteractionSystemConfiguration>{

    ConfigurationRSX4.customer1.dependencies.add(ConfigurationRSX4.frontend) // Visuelle Unterstützung

    ConfigurationRSX4.frontend.backend = ConfigurationRSX4.backend
    ConfigurationRSX4.frontend.dependencies.add(ConfigurationRSX4.backend) // Visuelle Unterstützung

    ConfigurationRSX4.backend.database = ConfigurationRSX4.database
    ConfigurationRSX4.backend.dependencies.add(ConfigurationRSX4.database) // Visuelle Unterstützung

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = ConfigurationRSX4.environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            ConfigurationRSX4.customer1,
            ConfigurationRSX4.frontend,
            ConfigurationRSX4.backend,
            ConfigurationRSX4.database),
        testScenario =
        scenario{
            label("Init")
            request(ConfigurationRSX4.customer1 sends ConfigurationRSX4.frontend.requestOffer(
                ConfigurationRSX4.startPos,
                ConfigurationRSX4.destination,
                1,
                ConfigurationRSX4.customer1.name,
            ))
            val responseEvent = waitFor(ConfigurationRSX4.frontend sends ConfigurationRSX4.customer1 receives ICustomer::frontendOfferResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            request(ConfigurationRSX4.customer1 sends ConfigurationRSX4.frontend.rejectOffer(responseValue))
        },
        guaranteeScenarios = setOf(vehicleRsClientScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Requesting ride offer and reject it" to configuration
}
