package de.fhdw.ridesharingservice.cases.userLoginConfiguration

import de.fhdw.ridesharingservice.classes.*
import de.fhdw.ridesharingservice.scenarios.userLoginScenarios.userLoginScenarios
import de.fhdw.ridesharingservice.scenarios.userLoginScenarios.userRegistrationScenarios
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.sends
import org.scenariotools.smlk.symbolicEvent
import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.smlkextension.label
import org.scenriotools.smlk.animator.smlkextension.shadowScenario

val customer = Customer("User", 50.0, 50.0)

val frontend = Frontend("RSS-Frontend", 300.0, 50.0)

val loginService = LoginService("Login-Service", 550.0, 50.0)

val identyAccessManagement = IdentityAccessManagementService("Access-Management", 550.0, 200.0)


var environmentMessageTypes = listOf(
    Frontend::login,
    Frontend::register)

fun registerDependencies(){
    customer.dependencies.add(frontend) // Visuelle Unterstützung

    frontend.dependencies.add(loginService) // Visuelle Unterstützung

    loginService.dependencies.add(identyAccessManagement) // Visuelle Unterstützung

    //lege Service fuer LoginFlow fest
    frontend.loginService = loginService

    //lege Service fuer Authentifizierung fest
    loginService.identityAccessService = identyAccessManagement
}

fun userLoginFlowConfiguration() : Pair<String, InteractionSystemConfiguration>{
    registerDependencies()
    identyAccessManagement.registerUser("testUser2", "pw2")
    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            customer,
            frontend,
            loginService,
            identyAccessManagement
        ),
        testScenario =
        scenario{
            label("Login user with name testUser: error")
            request(customer sends frontend.login("testUser", "pw"))
            label("Login user with name testUser: success")
            request(customer sends frontend.login("testUser2", "pw2"))
        },
        guaranteeScenarios = setOf(userLoginScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Login a user" to configuration
}

fun userRegistrationFlowConfiguration() : Pair<String, InteractionSystemConfiguration>{
    registerDependencies()

    val configuration = InteractionSystemConfiguration(
        1900.0, 900.0,
        environmentMessageTypes = environmentMessageTypes,
        eventsNotToVisualize = Instance::setShadowSize.symbolicEvent(),
        instances = listOf(
            customer,
            frontend,
            loginService,
            identyAccessManagement
        ),
        testScenario =
        scenario{
            label("Register user with name testUser")
            request(customer sends frontend.register("testUser", "pw"))
            label("Register user with name testUser2")
            request(customer sends frontend.register("testUser2", "pw2"))
        },
        guaranteeScenarios = setOf(userRegistrationScenarios, setOf(shadowScenario)).flatten().toSet()
    )

    return "Demo RSS -- Register a new user" to configuration
}
