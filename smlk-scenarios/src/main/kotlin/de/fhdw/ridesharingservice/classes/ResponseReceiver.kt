package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event

interface ResponseReceiver {
    fun receiveLoginResponse(token: String) = event(token){}

    fun receiveRegistrationResponse(message: String) = event(message){}
}
