package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

class Customer(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), ICustomer {
}

interface ICustomer{
    fun frontendPositionResponse(position: Position) = event(position){}
    fun frontendOfferResponse(offer: Booking) = event(offer){}
    fun frontendBookingConfimationResponse(booking: Booking) = event(booking){}
    fun frontendBookingRejectedResponse(booking: Booking) = event(booking) {}
    fun frontendAllVehiclesResponse(vehicles: Collection<Vehicle>) = event(vehicles){}

}
