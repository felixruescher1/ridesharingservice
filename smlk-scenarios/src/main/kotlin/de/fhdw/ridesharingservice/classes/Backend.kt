package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

class Backend(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), IBackend {

    var database: Database? = null

    fun currentPosition(vehicleId: Int, long: Int, lat: Int) = event(vehicleId, long, lat) {
        database!!.currentPosition(vehicleId, long, lat)
    }

    fun getInformationOfAllVehicles() = event {}

    fun getPositionOfRide(vehicleId: Int) = event(vehicleId) {}

    fun calculateOffer(start: Position, destination: Position, bookedSeats: Int, name: String) = event(start, destination, bookedSeats, name) {}

    fun bookOffer(offer: Booking) = event(offer){}

    fun rejectOffer(offer: Booking) = event(offer){}
}

interface IBackend{
    fun databasePositionResponse(position: Position) = event(position){}
    fun databasePossibleVehicleResponse(vehicle: Vehicle) = event(vehicle) {}
    fun databaseBookingConfirmationResponse(booking: Booking) = event(booking){}
    fun databaseAllVehicleResponse(vehicles: Collection<Vehicle>) = event(vehicles) {}
}
