package de.fhdw.ridesharingservice.classes

import java.math.BigInteger

data class Booking(val price: Double, val start: Position, val destination: Position, val vehicleId: Int) {

}
