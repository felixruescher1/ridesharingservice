package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

// Fahrzeug von außen (in der Umwelt)
class Vehicle(val id : Int, xPos : Double, yPos : Double, seats: Int)
    : Instance("Vehicle $id", xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")) {
}
