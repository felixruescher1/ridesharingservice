package de.fhdw.ridesharingservice.classes

import javafx.beans.binding.StringBinding
import javafx.beans.property.SimpleMapProperty
import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c
import tornadofx.toObservable

//TODO: placeholder class --> active direcotry or keycloak server
//TODO: return token with user informations (roles, etc.)
class IdentityAccessManagementService(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")) {

    private val userCredentialsProperty = SimpleMapProperty(mapOf<String, String>().toObservable())

    fun login(username: String, password: String) = event(username, password){}
    fun register(username: String, password: String) = event(username, password){}

    fun validate(username: String, password: String): String {
        var response: String = "User successfull authenticated"
        println("Login User: $username")
        var userPassword: String? = userCredentialsProperty[username]
        if (userPassword == null || userPassword != password) {
            response = "Unauthorized"
            //TODO: call rest service

        }
        return response
    }

    fun registerUser(username: String, password: String): String {
        var response: String = "Registration successfull"
        if(!userCredentialsProperty.containsKey(username)){
            userCredentialsProperty[username] = password
            println(userCredentialsProperty)
        }else{
            response = "user already exists"
        }
        return response
    }
    //TODO: nach put, erfolgt JavaFx exception und map wird nciht aktualisiert
    // Dient der Visualisierung im Frontend
  override fun getPropertyList(): List<Pair<String, StringBinding>> {
        return  listOf(
            "known users" to userCredentialsProperty.asString()
        )
    }
}
