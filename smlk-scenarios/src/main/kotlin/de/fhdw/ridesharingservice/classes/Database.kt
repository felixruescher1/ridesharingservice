package de.fhdw.ridesharingservice.classes

import de.fhdw.ridesharingservice.repository.PositionRepositoryImpl
import javafx.beans.binding.StringBinding
import javafx.beans.property.SimpleMapProperty
import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c
import tornadofx.toObservable

class Database(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")) {

    val vehicleToLastPositionProperty = SimpleMapProperty(mapOf<Int, Position>().toObservable())
    val repository = PositionRepositoryImpl()

    fun currentPosition(vehicleId: Int, long: Int, lat: Int) = event(vehicleId, long, lat) {
        val position = Position(long, lat);
        vehicleToLastPositionProperty.put(vehicleId, position)
        repository.add(de.fhdw.ridesharingservice.domain.Position(lat, long))
        println(vehicleToLastPositionProperty)
    }

    fun getPositionOfRide(id: Int) = event(id) {
    }

    fun getInformationOfAllVehicles() = event {
        println("informationOfAllVehicles")
    }

    fun findPossibleVehicle(start: Position, destination: Position, bookedSeats: Int) = event(start, destination, bookedSeats) {

    }

    fun bookVehicle(booking: Booking) = event(booking){}

    // Dient der Visualisierung im Frontend
    override fun getPropertyList(): List<Pair<String, StringBinding>> {
        return  listOf(
            "vehicle positions" to vehicleToLastPositionProperty.asString()
        )
    }
}
