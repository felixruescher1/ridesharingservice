package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

class Frontend(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), ResponseReceiver, IFrontend {


    var loginService: LoginService? = null;
    var backend: Backend? = null;

    fun login(username: String, password: String) = event(username, password){}

    fun register(username: String, password: String) = event(username, password){}
    // Environment-Event (Frontend ruft diese Funktion auf)
    // Parameter an Event weitergeben
    fun getInformationOfAllVehicles() = event {}

    fun getPositionOfRide(vehicleId: Int) = event(vehicleId) {
    }

    fun requestOffer(start: Position, destination: Position, bookedSeats: Int, name: String) = event(start, destination, bookedSeats, name) {}

    fun bookOffer(offer: Booking) = event(offer){}

    fun rejectOffer(offer: Booking) = event(offer){}
}

interface IFrontend{
    fun backendPositionResponse(position: Position) = event(position){}
    fun backendOfferResponse(offer: Booking) = event(offer){}
    fun backendBookingConfirmationResponse(booking: Booking) = event(booking){}
    fun backendRejectOfferResponse(offer: Booking) = event(offer){}
    fun backendAllVehiclesResponse(vehicles: Collection<Vehicle>) = event(vehicles){}
}
