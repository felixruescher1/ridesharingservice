package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

// Softwarekomponente, die im Auto läuft (IoT)
class VehicleRSClient(val id : Int, xPos : Double, yPos : Double)
    : Instance("Vehicle RSClient $id", xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")) {

    var backend: Backend? = null;

    // Environment-Event (Client-Software ruft diese Funktion auf)
    // Parameter an Event weitergeben
    fun currentPosition(long: Int, lat: Int) = event(long, lat) {

    }

}