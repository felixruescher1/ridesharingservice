package de.fhdw.ridesharingservice.classes

import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.c

class LoginService(name : String, xPos : Double, yPos : Double)
    : Instance(name, xPos, yPos, width = 200.0, height = 60.0, color = c("#000000"), bgcolor = c("#eeffaa")), ResponseReceiver {

    var identityAccessService: IdentityAccessManagementService? = null

    fun login(username: String, password: String) = event(username, password){}

    fun register(username: String, password: String) = event(username, password){}
}
