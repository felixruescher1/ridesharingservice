package de.fhdw.ridesharingservice.scenarios

import de.fhdw.ridesharingservice.classes.*
import org.scenariotools.smlk.*

val demoPosition = Position(42, 42)

// Sequence-Diagram: Existentielle Sequenzen
val vehicleRsClientScenarios = listOf(

    //Szenario-basierte Regel
    scenario(ANY sends VehicleRSClient::currentPosition.symbolicEvent()) {
        val vehicleRSClient = it.receiver
        val long = it.parameters[0] as Int
        val lat = it.parameters[1] as Int

        if (vehicleRSClient.backend != null)
            request(vehicleRSClient sends vehicleRSClient.backend!!.currentPosition(vehicleRSClient.id, long, lat))
    },

    scenario(ANY sends Backend::currentPosition.symbolicEvent()) {
        val backend = it.receiver
        val vehicleId = it.parameters[0] as Int
        val long = it.parameters[1] as Int
        val lat = it.parameters[2] as Int

        if (backend.database != null)
            request(backend sends backend.database!!.currentPosition(vehicleId, long, lat))
    },

    scenario(ANY sends Frontend::getInformationOfAllVehicles.symbolicEvent()) {
        val frontend = it.receiver
        val requestingAdmin = it.sender as ICustomer
        if (frontend.backend != null) {
            request(frontend sends frontend.backend!!.getInformationOfAllVehicles())
            val responseEvent = waitFor(ANY sends frontend receives IFrontend::backendAllVehiclesResponse)
            val responseValue = responseEvent.parameters[0] as Collection<Vehicle>
            println("Received in backend: $responseValue")

            request(frontend sends requestingAdmin.frontendAllVehiclesResponse(responseValue))
        }

    },

    scenario(ANY sends Backend::getInformationOfAllVehicles.symbolicEvent()) {
        val backend = it.receiver
        val requestingFrontend = it.sender as IFrontend
        if (backend.database != null) {
            request(backend sends backend.database!!.getInformationOfAllVehicles())
            val responseEvent = waitFor(ANY sends backend receives IBackend::databaseAllVehicleResponse)
            val responseValue = responseEvent.parameters[0] as Collection<Vehicle>
            println("Received in backend: $responseValue")

            request(backend sends requestingFrontend.backendAllVehiclesResponse(responseValue))
        }

    },

    scenario(ANY sends Frontend::getPositionOfRide.symbolicEvent()){

        val vehicleId = it.parameters[0] as Int
        val frontend = it.receiver
        val requestingCustomer = it.sender as ICustomer

        if (frontend.backend != null) {
            request(frontend sends frontend.backend!!.getPositionOfRide(vehicleId))
            val responseEvent = waitFor(ANY sends frontend receives IFrontend::backendPositionResponse)
            val responseValue = responseEvent.parameters[0] as Position
            println("Received in frontend: $responseValue")

            request(frontend sends requestingCustomer.frontendPositionResponse(responseValue))
        }
    },

    scenario(ANY sends Backend::getPositionOfRide.symbolicEvent()){

        val vehicleId = it.parameters[0] as Int
        val backend = it.receiver
        val requestingFrontend = it.sender as IFrontend

        if (backend.database != null) {
            request(backend sends backend.database!!.getPositionOfRide(vehicleId))
            val responseEvent = waitFor(ANY sends backend receives IBackend::databasePositionResponse)
            val responseValue = responseEvent.parameters[0] as Position
            println("Received in backend: $responseValue")

            request(backend sends requestingFrontend.backendPositionResponse(responseValue))
        }
    },

    scenario(ANY sends Database::getPositionOfRide.symbolicEvent()){

        val vehicleId = it.parameters[0] as Int
        val database = it.receiver
        val requestingBackend = it.sender as IBackend

        val position = database.vehicleToLastPositionProperty[vehicleId]

        request(database sends requestingBackend.databasePositionResponse(position ?: demoPosition))
    },

    scenario(ANY sends Frontend::requestOffer.symbolicEvent()){
        val start = it.parameters[0] as Position
        val destination = it.parameters[1] as Position
        val bookedSeats = it.parameters[2] as Int
        val name = it.parameters[3] as String

        val frontend = it.receiver
        val requestingCustomer = it.sender as ICustomer

        if (frontend.backend != null) {
            request(frontend sends frontend.backend!!.calculateOffer(start, destination, bookedSeats, name))
            val responseEvent = waitFor(ANY sends frontend receives IFrontend::backendOfferResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            println("Received in frontend: $responseValue")

            request(frontend sends requestingCustomer.frontendOfferResponse(responseValue))
        }
    },

    scenario(ANY sends Backend::calculateOffer.symbolicEvent()){
        val start = it.parameters[0] as Position
        val destination = it.parameters[1] as Position
        val bookedSeats = it.parameters[2] as Int

        val backend = it.receiver
        val requestingFrontend = it.sender as IFrontend

        if (backend.database != null) {
            request(backend sends backend.database!!.findPossibleVehicle(start, destination, bookedSeats))
            val responseEvent = waitFor(ANY sends backend receives IBackend::databasePossibleVehicleResponse)
            val responseValue = responseEvent.parameters[0] as Vehicle
            println("Received in backend: $responseValue")
            val offer: Booking = Booking(5.0, start, destination, responseValue.id)
            request(backend sends requestingFrontend.backendOfferResponse(offer))
        }
    },

    scenario(ANY sends Database::findPossibleVehicle.symbolicEvent()){

        val start = it.parameters[0] as Position
        val destination = it.parameters[1] as Position
        val bookedSeats = it.parameters[2] as Int
        val database = it.receiver
        val requestingBackend = it.sender as IBackend

        val vehicle = Vehicle(1, 40.0, 50.0, 4)//database.vehicleToLastPositionProperty[vehicleId]

        request(database sends requestingBackend.databasePossibleVehicleResponse(vehicle))
    },

    scenario(ANY sends Frontend::bookOffer.symbolicEvent()){
        val offer = it.parameters[0] as Booking

        val frontend = it.receiver
        val requestingCustomer = it.sender as ICustomer

        if (frontend.backend != null) {
            request(frontend sends frontend.backend!!.bookOffer(offer))
            val responseEvent = waitFor(ANY sends frontend receives IFrontend::backendBookingConfirmationResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            println("Received in frontend: $responseValue")

            request(frontend sends requestingCustomer.frontendBookingConfimationResponse(responseValue))
        }
    },

    scenario(ANY sends Backend::bookOffer.symbolicEvent()){
        val offer = it.parameters[0] as Booking

        val backend = it.receiver
        val requestingFrontend = it.sender as IFrontend

        if (backend.database != null) {
            request(backend sends backend.database!!.bookVehicle(offer))
            val responseEvent = waitFor(ANY sends backend receives IBackend::databaseBookingConfirmationResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            println("Received in backend: $responseValue")

            request(backend sends requestingFrontend.backendBookingConfirmationResponse(responseValue))
        }
    },

    scenario(ANY sends Database::bookVehicle.symbolicEvent()){

        val booking = it.parameters[0] as Booking
        val database = it.receiver
        val requestingBackend = it.sender as IBackend
        // some booking logic here
        println("Booked vehicle: ${booking.vehicleId} for booking")
        request(database sends requestingBackend.databaseBookingConfirmationResponse(booking))
    },

    scenario(ANY sends Frontend::rejectOffer.symbolicEvent()){
        val offer = it.parameters[0] as Booking

        val frontend = it.receiver
        val requestingCustomer = it.sender as ICustomer

        if (frontend.backend != null) {
            request(frontend sends frontend.backend!!.rejectOffer(offer))
            val responseEvent = waitFor(ANY sends frontend receives IFrontend::backendRejectOfferResponse)
            val responseValue = responseEvent.parameters[0] as Booking
            println("Received in frontend: $responseValue")

            request(frontend sends requestingCustomer.frontendBookingRejectedResponse(responseValue))
        }
    },

    scenario(ANY sends Backend::rejectOffer.symbolicEvent()){
        val offer = it.parameters[0] as Booking

        val backend = it.receiver
        val requestingFrontend = it.sender as IFrontend

        // some logic here
        request(backend sends requestingFrontend.backendRejectOfferResponse(offer))
    },

    scenario(ANY sends Database::getInformationOfAllVehicles.symbolicEvent()){

        val database = it.receiver
        val requestingBackend = it.sender as IBackend

        // some logic here
        val veh1: Vehicle = Vehicle(1, 50.0, 100.0, 5)
        val veh2: Vehicle = Vehicle(2, 250.0, 100.0, 5)
        request(database sends requestingBackend.databaseAllVehicleResponse(listOf(veh1, veh2)))
    }
)
