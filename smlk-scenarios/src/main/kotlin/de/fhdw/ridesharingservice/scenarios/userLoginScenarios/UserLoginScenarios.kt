package de.fhdw.ridesharingservice.scenarios.userLoginScenarios

import de.fhdw.ridesharingservice.classes.Frontend
import de.fhdw.ridesharingservice.classes.IdentityAccessManagementService
import de.fhdw.ridesharingservice.classes.LoginService
import de.fhdw.ridesharingservice.classes.ResponseReceiver
import org.scenariotools.smlk.*

import org.scenriotools.smlk.animator.smlkextension.instancelabel

// Sequence-Diagram: Existentielle Sequenzen
val userLoginScenarios = listOf(

    //Szenario-basierte Regel
    scenario(ANY sends Frontend::login.symbolicEvent()) {
        val frontendClient = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String

        if (frontendClient.loginService != null){
            instancelabel(frontendClient,"Request login at login service")
            request(frontendClient sends frontendClient.loginService!!.login(username, password))
            instancelabel(frontendClient,"Waiting for login service response")
            val responseEvent = waitFor(frontendClient receives ResponseReceiver::receiveLoginResponse)
            val responseValue = responseEvent.parameters[0] as String
            if(responseValue == "Unauthorized"){
                instancelabel(frontendClient, "Authentification failed for user with id $username")
            }else{
                instancelabel(frontendClient, "Authentification successfull")
            }

        }

    },

    scenario(ANY sends LoginService::login.symbolicEvent()) {
        val loginService = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String
        val requestingClient = it.sender as ResponseReceiver

        if (loginService.identityAccessService != null){
            instancelabel(loginService,"Request login at iam")
            request(loginService sends loginService.identityAccessService!!.login(username, password))
            instancelabel(loginService,"Waiting for iam response")
            val responseEvent = waitFor(loginService receives ResponseReceiver::receiveLoginResponse)
            val responseValue = responseEvent.parameters[0] as String

            instancelabel(loginService, "Send result back to frontend")
            request(loginService sends requestingClient.receiveLoginResponse(responseValue))
        }
    },

    scenario(ANY sends IdentityAccessManagementService::login.symbolicEvent()) {
        val iam = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String
        val requestingClient = it.sender as ResponseReceiver
        //TODO:
        //request(... sends externalValService.valida)
        val response = iam.validate(username, password)

        instancelabel(iam, "Authentification finished")
        request(iam sends requestingClient.receiveLoginResponse(response))

    }
    // MockßScenario (designßtime)
    //scenario (... receives .validate)
    // request (...isValid)

    // Emitter scenario (runßtime)
    //scenario(... receives validate)
    // (MAKE REST CALL)
    // (RECEIVE RESPONSE)
    // (WRAP RESPONSE to Object event) request(... (depends o))

    // in spring controller URL mapping auf java/KotlinßMethode

)

val userRegistrationScenarios = listOf(
    scenario(ANY sends Frontend::register.symbolicEvent()) {
        val frontendClient = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String

        if (frontendClient.loginService != null){
            instancelabel(frontendClient,"Request registration at login service")
            request(frontendClient sends frontendClient.loginService!!.register(username, password))
            instancelabel(frontendClient,"Waiting for login service response")
            val responseEvent = waitFor(frontendClient receives ResponseReceiver::receiveRegistrationResponse)
            val responseValue = responseEvent.parameters[0] as String
            if(responseValue.contains("user already exists")){
                instancelabel(frontendClient, "ERROR: the username $username is already taken")
            }else{
                instancelabel(frontendClient, responseValue)
            }

        }
    },

    scenario(ANY sends LoginService::register.symbolicEvent()) {
        val registrationService = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String
        val requestingClient = it.sender as ResponseReceiver

        if (registrationService.identityAccessService != null){
            instancelabel(registrationService,"Request registration at iam")
            request(registrationService sends registrationService.identityAccessService!!.register(username, password))
            instancelabel(registrationService,"Waiting for iam response")
            val responseEvent = waitFor(registrationService receives ResponseReceiver::receiveRegistrationResponse)
            val responseValue = responseEvent.parameters[0] as String

            //instancelabel(registrationService, "Send result back to frontend")
            request(registrationService sends requestingClient.receiveRegistrationResponse(responseValue))
        }
    },

    scenario(ANY sends IdentityAccessManagementService::register.symbolicEvent()) {
        val iamRegister = it.receiver
        val username = it.parameters[0] as String
        val password = it.parameters[1] as String
        val requestingClient = it.sender as ResponseReceiver
        val response = iamRegister.registerUser(username, password)

        //instancelabel(iamRegister, "Registration finished")
        request(iamRegister sends requestingClient.receiveRegistrationResponse(response))

    }

)
