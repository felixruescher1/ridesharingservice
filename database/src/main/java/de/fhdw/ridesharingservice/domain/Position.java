package de.fhdw.ridesharingservice.domain;

import javax.persistence.*;

@Entity
@Table(name = "POSITIONS")
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "LATITUDE")
    private int latitude;

    @Column(name = "LONGITUDE")
    private int longitude;

    public Position() {
    }

    public Position(final int latitude, final int longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}


