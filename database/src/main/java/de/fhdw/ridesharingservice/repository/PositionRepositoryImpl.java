package de.fhdw.ridesharingservice.repository;

import de.fhdw.ridesharingservice.domain.Position;

public final class PositionRepositoryImpl extends Repository<Position> {

    @Override
    public Class<Position> getClazz() {
        return Position.class;
    }
}