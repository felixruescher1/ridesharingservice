package de.fhdw.ridesharingservice.repository;

import de.fhdw.ridesharingservice.domain.Position;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

abstract class Repository<T> {

    private static final SessionFactory SESSION_FACTORY = new Configuration()
            .configure()
            .addAnnotatedClass(Position.class)
            .buildSessionFactory();

    public T getById(final Session session, int id) {
        return session.get(this.getClazz(), id);
    }

    public void add(T entity) {
        add(this.createSession(), entity);
    }

    public void add(final Session session, T entity) {
        System.out.println("Persisting object in database");
        final Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
    }

    public abstract Class<T> getClazz();

    public final Session createSession() {
        return SESSION_FACTORY.openSession();
    }

}
