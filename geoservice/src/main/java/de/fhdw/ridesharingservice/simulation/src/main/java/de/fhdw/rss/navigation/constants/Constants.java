package de.fhdw.rss.navigation.constants;

public class Constants {
	
	public static final String URL_OPENROUTESERVICE = "https://api.openrouteservice.org/";
	public static final String URL_SEARCH = URL_OPENROUTESERVICE + "geocode/search?api_key=%s&text=%s&boundary.circle.lon=%s&boundary.circle.lat=%s&boundary.circle.radius=%s&layers=address";
	public static final String URL_NAVIGATION_INFORMATION = URL_OPENROUTESERVICE + "v2/directions/%s?api_key=%s&start=%s,%s&end=%s,%s";
	public static final String URL_COORDINATES = URL_OPENROUTESERVICE + "geocode/search?api_key=%s&text=%s";
	
	public static final String API_KEY = "5b3ce3597851110001cf6248243b8f7ed23a4afeb47fb082ec999536";
	
	public static final String[] PROFILES = {"driving-car", "driving-hgv", "cycling-regular", "cycling-road", "cycling-mountain", "cycling-electric", "foot-walking", "foot-hiking", "wheelchair"};

	public static final String LONGITUDE_NEW_TOWN_HALL = "9.738118900751012";
	public static final String LATITUDE_NEW_TOWN_HALL = "52.37455354980162";
	
	public static final String DEFAULT_RANDOM_LOCATION = "Freundallee 15, 30173, Hannover";
	
}
