package de.fhdw.rss.navigation.simulation;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.fhdw.rss.navigation.constants.Constants;
import de.fhdw.rss.navigation.controller.UtilsForNavigation;
import de.fhdw.rss.navigation.model.Journey;
import de.fhdw.rss.navigation.model.NavigationInformation;
import de.fhdw.rss.navigation.model.Vehicle;

public class Simulation {
	
	public static void main(String[] args) throws IOException {
		String locationStartJ1 = UtilsForNavigation.getRandomLocation(Constants.LONGITUDE_NEW_TOWN_HALL, Constants.LATITUDE_NEW_TOWN_HALL, 2);
		String longitudeStart = UtilsForNavigation.getLongitudeAndLatitudeOfLocation(locationStartJ1)[0];
		String latitudeStart = UtilsForNavigation.getLongitudeAndLatitudeOfLocation(locationStartJ1)[1];
		System.out.println("Startpunkt Fahrt1: " + locationStartJ1);
		String locationEndJ1 = UtilsForNavigation.getRandomLocation(Constants.LONGITUDE_NEW_TOWN_HALL, Constants.LATITUDE_NEW_TOWN_HALL, 2);
		String longitudeEnd = UtilsForNavigation.getLongitudeAndLatitudeOfLocation(locationEndJ1)[0];
		String latitudeEnd = UtilsForNavigation.getLongitudeAndLatitudeOfLocation(locationEndJ1)[1];
		System.out.println("Endpunkt Fahrt1: " + locationEndJ1);
		
		Journey j1 = new Journey(locationStartJ1, locationEndJ1, 2, 0);
		Vehicle vehicle = UtilsForNavigation.getMostIdealVehicle(j1);
		System.out.println("Beauftragtes Auto (kürzeste Fahrtzeit zum Kunden): " + vehicle.getId());
		System.out.println("Auto " + vehicle.getId() + " fährt von seinem Standort zum Kunden");

		NavigationInformation ni = UtilsForNavigation.getNavigationInformationBetweenToPoints(Constants.PROFILES[0], vehicle.getLongitude(), vehicle.getLatitude(), longitudeStart, latitudeStart);
		Simulation.simulateJourney(ni);
	}
	
	public static void simulateJourney(NavigationInformation ni) {
//		System.out.println("Anzahl Schritte: " + ni.getCoordinates().size());
		Double duration = ni.getDuration();
		System.out.println("Gesamte Fahrtzeit: " + duration);
		Double distance = ni.getDistance();
		System.out.println("Gesamte Distanz: " + distance);
		List<Map<String, String>> steps =  ni.getSteps();
		for (Map<String, String> step : steps) {
			Double durationStep = Double.parseDouble(step.get("duration"));
//			System.out.println("Dauer Abschnitt: " + durationStep);
			Double distanceStep = Double.parseDouble(step.get("distance"));
//			System.out.println("Strecke Abschnitt: " + distanceStep);
			Integer rideStepFromPart = Integer.parseInt(step.get("way_points").split(",")[0].replace("[", ""));
//			System.out.println("Abschnitt ab Schritt: " + rideStepFromPart);
			Integer rideSteptToPart = Integer.parseInt(step.get("way_points").split(",")[1].replace("]", ""));
//			System.out.println("Abschnitt bis Schritt: " + rideSteptToPart);
			Integer ridePartsStep = rideSteptToPart - rideStepFromPart;
//			System.out.println("Anzahl Schritte Abschnitt: " + ridePartsStep);
			for (int i=rideStepFromPart; i<rideSteptToPart; i++) {
				System.out.println("Aktueller Standort: " + ni.getCoordinates().get(i)[0] + "," + ni.getCoordinates().get(i)[1] + ": " + step.get("name"));
				duration = duration - (durationStep / ridePartsStep);
				System.out.println("Verbleibende Fahrtzeit: " + duration.intValue());
				distance = distance - (distanceStep / ridePartsStep);
				System.out.println("Verbleibende Distanz: " + distance.intValue());
				try {
					Thread.sleep((durationStep.intValue() * 1000) / ridePartsStep);
				} catch (InterruptedException e) {
				}
			}
		}
		System.out.println("Ziel erreicht");
	}

}
