package de.fhdw.rss.navigation.controller;

import de.fhdw.rss.navigation.constants.Constants;
import de.fhdw.rss.navigation.model.Carpool;
import de.fhdw.rss.navigation.model.Journey;
import de.fhdw.rss.navigation.model.NavigationInformation;
import de.fhdw.rss.navigation.model.Vehicle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class UtilsForNavigation {
	
	public static Vehicle getMostIdealVehicle(Journey journey) throws IOException {
		Vehicle mostIdealVehicle = null;
		String[] longitudeAndLatitudeStart = UtilsForNavigation.getLongitudeAndLatitudeOfLocation(journey.getLocationStart());
		String longitudeStart = longitudeAndLatitudeStart[0];
//		System.out.println(longitudeStart);
		String latitudeStart = longitudeAndLatitudeStart[1];
//		System.out.println(latitudeStart);
		Double duration = 3601.0;
		for (Vehicle vehicle : Carpool.getInstance().getVehicles()) {
			if (journey.getPassengers()<=(vehicle.getMaximumPassengers() - vehicle.getActualPassengers())) {
				String longitudeVehicle = vehicle.getLongitude();
//				System.out.println(longitudeVehicle);
				String latitudeVehicle = vehicle.getLatitude();
//				System.out.println(latitudeVehicle);
				Double durationForVehicle = UtilsForNavigation.getNavigationInformationBetweenToPoints(vehicle.getProfile(), longitudeStart, latitudeStart, longitudeVehicle, latitudeVehicle).getDuration();
//				System.out.println(durationForVehicle);
//				System.out.println(vehicle.getId());
				if (durationForVehicle<duration) {
					duration = durationForVehicle;
					mostIdealVehicle = vehicle;
				}
			}
		}
		return mostIdealVehicle;
	}
	
	public static String getRandomLocation(String longitude, String latitude, Integer radius) throws IOException {
		String apiKey = Constants.API_KEY;
	    Random random = new Random();
	    Integer generatedNumber = random.ints(1, 101).findFirst().getAsInt();
		String location = generatedNumber.toString();

		URL request = new URL(String.format(Constants.URL_SEARCH, apiKey, location, longitude, latitude, radius));
//		System.out.println(request);
		String response = getResponseFormURL(request);
//		System.out.println(response);
		
		JSONObject jsonObject = new JSONObject(response);
		JSONArray features = jsonObject.getJSONArray("features");
		JSONObject firstOfFeatures = (JSONObject) features.get(0);
		JSONObject properties = (JSONObject) firstOfFeatures.get("properties");
		String street = properties.get("street").toString();
		String housenumber = properties.get("housenumber").toString();
		String postalcode = properties.get("postalcode").toString();
		String randomLocation = street + " " + housenumber + ", " + postalcode;
		
		return randomLocation;
	}
	
	public static NavigationInformation getNavigationInformationBetweenTwoLocations(String profile, String locationStart, String locationEnd) throws IOException {
		String[] longitudeAndLatitudeStart = getLongitudeAndLatitudeOfLocation(locationStart);
		String[] longitudeAndLatitudeEnd = getLongitudeAndLatitudeOfLocation(locationEnd);
		String longitudeStart = longitudeAndLatitudeStart[0];
		String latitudeStart = longitudeAndLatitudeStart[1];
		String longitudeEnd = longitudeAndLatitudeEnd[0];
		String latitudeEnd = longitudeAndLatitudeEnd[1];
		NavigationInformation ni = getNavigationInformationBetweenToPoints(profile, longitudeStart, latitudeStart, longitudeEnd, latitudeEnd);
		return ni;
	}
	
	public static NavigationInformation getNavigationInformationBetweenToPoints(String profile, String longitudeStart, String latitudeStart, String longitudeEnd, String latitudeEnd) throws IOException {
		String apiKey = Constants.API_KEY;

		URL request = new URL(String.format(Constants.URL_NAVIGATION_INFORMATION, profile, apiKey, longitudeStart, latitudeStart, longitudeEnd, latitudeEnd));
//		System.out.println(request);
		String response = getResponseFormURL(request);
//		System.out.println(response);
		
		NavigationInformation ni = new NavigationInformation();
		ni.setProfile(profile);
		ni.setLongitudeStart(longitudeStart);
		ni.setLatitudeStart(latitudeStart);
		ni.setLongitudeEnd(longitudeEnd);
		ni.setLatitudeEnd(latitudeEnd);
		
		JSONObject jsonObject = new JSONObject(response);
		JSONArray features = jsonObject.getJSONArray("features");
		JSONObject firstOfFeatures = (JSONObject) features.get(0);

		JSONObject geometry = (JSONObject) firstOfFeatures.get("geometry");
		JSONArray coordinatesAsJSONArray = geometry.getJSONArray("coordinates");
		List<String[]> coordinatesAsList = new ArrayList<String[]>();
		for (Object coordinate : coordinatesAsJSONArray) {
			JSONArray coordinateAsJSONArray = (JSONArray) coordinate;
			String[] tupelOfCoordinatesAsString =  {coordinateAsJSONArray.get(0).toString(), coordinateAsJSONArray.get(1).toString()};
			coordinatesAsList.add(tupelOfCoordinatesAsString);
		}
		ni.setCoordinates(coordinatesAsList);

		JSONObject properties = (JSONObject) firstOfFeatures.get("properties");
		JSONArray segments = (JSONArray) properties.get("segments");
		JSONObject firstOfSegments = (JSONObject) segments.get(0);
		Double distance = ((BigDecimal) firstOfSegments.get("distance")).doubleValue();
		Double duration = ((BigDecimal) firstOfSegments.get("duration")).doubleValue();
		ni.setDistance(distance);
		ni.setDuration(duration);

		JSONArray stepsAsJSONArray = (JSONArray) firstOfSegments.get("steps");
		List<Map<String, String>> steps = new ArrayList<Map<String, String>>();
		for (Object step : stepsAsJSONArray) {
			JSONObject stepAsJSONObject = (JSONObject) step;
			Map<String, String> stepKeysAndValues = new HashMap<String, String>();
			for (String key : stepAsJSONObject.keySet()) {
				stepKeysAndValues.put(key, stepAsJSONObject.get(key).toString());
			}
			steps.add(stepKeysAndValues);
		}
		ni.setSteps(steps);
		
		return ni;
	}
	
	public static String[] getLongitudeAndLatitudeOfLocation(String location) throws IOException {
		String apiKey = Constants.API_KEY;

		location = URLEncoder.encode(location, StandardCharsets.UTF_8);
		URL request = new URL(String.format(Constants.URL_COORDINATES, apiKey, location.replaceAll(" ", "%20")));
//		System.out.println(request);
		String response = getResponseFormURL(request);
//		System.out.println(response);
		
		JSONObject jsonObject = new JSONObject(response);
		JSONArray features = jsonObject.getJSONArray("features");
		JSONObject firstOfFeatures = (JSONObject) features.get(0);

		JSONObject geometry = (JSONObject) firstOfFeatures.get("geometry");
		JSONArray coordinatesAsJSONArray = geometry.getJSONArray("coordinates");
		String [] longitudeAndLatitude = {coordinatesAsJSONArray.get(0).toString(), coordinatesAsJSONArray.get(1).toString()};
			
		return longitudeAndLatitude;
	}
	
    public static String getResponseFormURL(URL url) throws IOException {
        InputStream is = url.openStream();
        String response = "";
        String line = null;
        
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        while ((line=rd.readLine())!=null) {
        	response = response + line;
        }
        is.close();
        rd.close();
        return response;
  }
	
}
