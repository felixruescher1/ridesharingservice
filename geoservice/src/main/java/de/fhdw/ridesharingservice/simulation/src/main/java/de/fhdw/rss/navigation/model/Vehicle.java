package de.fhdw.rss.navigation.model;

public class Vehicle {
	
	private String id;
	private String profile;
	private Integer maximumPassengers;
	private Integer actualPassengers;
	private String longitude;
	private String latitude;
	
	public Vehicle(String id, String profile, Integer maximumPassengers, Integer actualPassengers, String longitude, String latitude) {
		this.id = id;
		this.profile = profile;
		this.maximumPassengers = maximumPassengers;
		this.actualPassengers = actualPassengers;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Integer getMaximumPassengers() {
		return maximumPassengers;
	}

	public void setMaximumPassengers(Integer maximumPassengers) {
		this.maximumPassengers = maximumPassengers;
	}

	public Integer getActualPassengers() {
		return actualPassengers;
	}

	public void setActualPassengers(Integer actualPassengers) {
		this.actualPassengers = actualPassengers;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
}
