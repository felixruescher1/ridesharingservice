package de.fhdw.rss.navigation.model;

import java.util.List;
import java.util.Map;

public class NavigationInformation {
	
	private String profile;
	private String longitudeStart;
	private String latitudeStart;
	private String longitudeEnd;
	private String latitudeEnd;
	private Double distance;
	private Double duration;
	private List<Map<String, String>> steps;
	private List<String[]> coordinates;
	
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getLongitudeStart() {
		return longitudeStart;
	}
	public void setLongitudeStart(String longitudeStart) {
		this.longitudeStart = longitudeStart;
	}
	public String getLatitudeStart() {
		return latitudeStart;
	}
	public void setLatitudeStart(String latitudeStart) {
		this.latitudeStart = latitudeStart;
	}
	public String getLongitudeEnd() {
		return longitudeEnd;
	}
	public void setLongitudeEnd(String longitudeEnd) {
		this.longitudeEnd = longitudeEnd;
	}
	public String getLatitudeEnd() {
		return latitudeEnd;
	}
	public void setLatitudeEnd(String latitudeEnd) {
		this.latitudeEnd = latitudeEnd;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getDuration() {
		return duration;
	}
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	public List<Map<String, String>> getSteps() {
		return steps;
	}
	public void setSteps(List<Map<String, String>> steps) {
		this.steps = steps;
	}
	public List<String[]> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(List<String[]> coordinates) {
		this.coordinates = coordinates;
	}
	
}
