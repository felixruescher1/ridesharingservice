package de.fhdw.rss.navigation.model;

public class Journey {
	
	private String locationStart;
	private String locationEnd;
	private Integer passengers;
	private Integer maximumStopovers;
	
	public Journey(String locationStart, String locationEnd, Integer passengers, Integer maximumStopovers) {
		this.locationStart = locationStart;
		this.locationEnd = locationEnd;
		this.passengers = passengers;
		this.maximumStopovers = maximumStopovers;
	}

	public String getLocationStart() {
		return locationStart;
	}

	public void setLocationStart(String locationStart) {
		this.locationStart = locationStart;
	}

	public String getLocationEnd() {
		return locationEnd;
	}

	public void setLocationEnd(String locationEnd) {
		this.locationEnd = locationEnd;
	}

	public Integer getPassengers() {
		return passengers;
	}

	public void setPassengers(Integer passengers) {
		this.passengers = passengers;
	}

	public Integer getMaximumStopovers() {
		return maximumStopovers;
	}

	public void setMaximumStopovers(Integer maximumStopovers) {
		this.maximumStopovers = maximumStopovers;
	}
	
}
