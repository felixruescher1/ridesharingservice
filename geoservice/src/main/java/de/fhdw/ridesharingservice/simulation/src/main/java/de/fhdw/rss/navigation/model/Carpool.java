package de.fhdw.rss.navigation.model;

import java.util.HashSet;
import java.util.Set;

import de.fhdw.rss.navigation.constants.Constants;

public class Carpool {
	
	private static Carpool theInstance;
	private Set<Vehicle> vehicles;
	
	private Carpool() {
		vehicles = new HashSet<Vehicle>();
		vehicles.add(new Vehicle("1", Constants.PROFILES[0], 4, 0, "9.599759829183597", "52.42671249536806"));
		vehicles.add(new Vehicle("2", Constants.PROFILES[0], 4, 0, "9.739979691787477", "52.451901735227075"));
		vehicles.add(new Vehicle("3", Constants.PROFILES[0], 8, 0, "9.809132085783585", "52.307616400852496"));
		vehicles.add(new Vehicle("4", Constants.PROFILES[0], 4, 0, "9.808172175866579", "52.386778350017934"));
		vehicles.add(new Vehicle("5", Constants.PROFILES[0], 2, 0, "9.795110478617492", "52.40137388170832"));
		vehicles.add(new Vehicle("6", Constants.PROFILES[0], 2, 0, "9.72781921800406", "52.400117042659666"));
		vehicles.add(new Vehicle("7", Constants.PROFILES[0], 4, 0, "9.758718266244921", "52.36197590923743"));
		vehicles.add(new Vehicle("8", Constants.PROFILES[0], 2, 0, "9.671514285654043", "52.36700739520916"));
		vehicles.add(new Vehicle("9", Constants.PROFILES[0], 2, 0, "9.760778202794311", "52.37916445408133"));
		vehicles.add(new Vehicle("10", Constants.PROFILES[0], 2, 0, "9.820516362726645", "52.36071794821025"));
	}

    public static Carpool getInstance() {
        if(theInstance==null) {
        	theInstance = new Carpool();
        }
        return theInstance;
    }
	
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
}
